# Minor project
__________________

### Group members
* Tony
* Akshay
* Sreelekshmi
* Annie

### Topic details
**Study+**, an EdTech platform  

Learning something new takes time, and some students may struggle to retain knowledge when there are numerous details to remember. To address this issue, spaced repetition and gamification of learning can be used, making studying more enjoyable and allowing us to quickly and repeatedly review our notes.  

Study+ will be built with HTML, CSS, PHP, MySQL for database functionality, and the Bootstrap framework to make it responsive on all devices.  

Students will have access to an easy-to-use, feature-rich website where they can watch videos created by our educators, study with flashcards for quick topic review, and take quizzes to test their understanding. All of these things will help them remember what they have learned. 


### To-do
- [ ] Initial Documentation for project (Rough ideas)
- [ ] About page
- [x] Setup docker
- [x] Project Abstract
- [ ] add animation on all pages

### Rough

- use bootstrap for designing website (front-end)

### Notes

- ``` docker run -it -p "80:80" -v ${PWD}/project/website:/app -v ${PWD}/mysql:/var/lib/mysql mattrayner/lamp:latest ```

### Resources

- https://www.freeformatter.com/html-formatter.html  
``` Format html files ```

- https://getbootstrap.com/docs/5.2/getting-started/introduction/  
```Bootstrap docs```

- https://www.youtube.com/watch?v=PxMjn9ii6ao  
``` Reference video```

- https://www.youtube.com/watch?v=veVZRLwfe6o  
``` Learning reference video ```

- https://hub.docker.com/r/mattrayner/lamp  
``` LAMP docker image ```

- https://www.geeksforgeeks.org/mysqli-procedural-functions/  
``` MySQLi reference ```

- https://getbootstrap.com/docs/5.0/examples/cheatsheet 
``` Bootstrap cheatsheet ```

- https://bootstrap-cheatsheet.themeselection.com/ 
``` Bootstrap cheatsheet ```